package blacknet

import (
	"bytes"
	"crypto/rand"
	"encoding/hex"
	"strings"

	"gitlab.com/blacknet-ninja/blacknetgo-lib/bech32"
	"gitlab.com/blacknet-ninja/blacknetgo-lib/bip39"
	"gitlab.com/blacknet-ninja/blacknetgo-lib/ed25519"
	"golang.org/x/crypto/blake2b"
	"golang.org/x/crypto/chacha20"
)

const (
	sigma = "Blacknet Signed Message:\n"
	hrp   = "blacknet"
)

// _publicKey account string to publickkey
func _publicKey(account string) ed25519.PublicKey {
	_, address, _ := bech32.Decode(account)
	pk, _ := bech32.ConvertBits(address, 5, 8, false)
	return pk
}

func _genRandomBytes(size int) []byte {
	blk := make([]byte, size)
	rand.Read(blk)
	return blk
}

// Hash blake2b hash
func Hash(str interface{}) []byte {
	var bys []byte
	switch b := str.(type) {
	case []byte:
		bys = b
	case string:
		bys = []byte(b)
	}
	bytes := blake2b.Sum256(bys)
	return bytes[:]
}

// NewMnemonic .
func NewMnemonic() string {
	return bip39.NewMnemonic()
}

// Ed25519 mnemonic string to ed25519
func Ed25519(mnemonic string) (ed25519.PublicKey, ed25519.PrivateKey, error) {
	hash := Hash(mnemonic)
	return ed25519.GenerateKey(bytes.NewReader(hash))
}

// Address mnemonic string to address
func Address(mnemonic string) string {
	pk, _, _ := Ed25519(mnemonic)
	convertbits, _ := bech32.ConvertBits(pk, 8, 5, true)
	address, _ := bech32.Encode(hrp, convertbits)
	return address
}

// Sign sign message
func Sign(mnemonic string, message string) string {
	hashMessage := Hash(sigma + message)
	_, sk, _ := Ed25519(mnemonic)
	signature := hex.EncodeToString(ed25519.Sign(sk, hashMessage))
	return strings.ToUpper(signature)
}

// Verify verify message
func Verify(account, signature, message string) bool {
	hashMessage := Hash(sigma + message)
	_, data, _ := bech32.Decode(account)
	pk, _ := bech32.ConvertBits(data, 5, 8, false)
	sign, _ := hex.DecodeString(signature)
	return ed25519.Verify(pk, hashMessage, sign)
}

// Signature  signature serialized
func Signature(mnemonic, serialized string) string {
	_, sk, _ := Ed25519(mnemonic)
	signedMessage, _ := hex.DecodeString(strings.ToLower(serialized))
	signedHash := Hash(signedMessage[64:len(signedMessage)])
	signature := ed25519.Sign(sk, signedHash)
	copy(signedMessage, signature[0:64])
	return hex.EncodeToString(signedMessage)
}

// Decrypt decrypt message
func Decrypt(mnemonic, account, encrypt string) string {
	pk := _publicKey(account)
	_, sk, _ := Ed25519(mnemonic)
	publicKey := ed25519.ConvertPublicKey(pk)
	privateKey := ed25519.ConvertPrivateKey(sk)
	key := ed25519.X25519(privateKey, publicKey)
	encryptByte, _ := hex.DecodeString(strings.ToLower(encrypt))
	IV := encryptByte[0:12]
	chacha20, _ := chacha20.NewUnauthenticatedCipher(key, IV)
	input := encryptByte[12:len(encryptByte)]
	dst := make([]byte, len(input))
	chacha20.XORKeyStream(dst, input)
	return string(dst)
}

// Encrypt encrypt message
func Encrypt(mnemonic, account, encrypt string) string {
	pk := _publicKey(account)
	_, sk, _ := Ed25519(mnemonic)
	publicKey := ed25519.ConvertPublicKey(pk)
	privateKey := ed25519.ConvertPrivateKey(sk)
	key := ed25519.X25519(privateKey, publicKey)
	IV := _genRandomBytes(12)
	input := []byte(encrypt)
	chacha20, _ := chacha20.NewUnauthenticatedCipher(key, IV)
	encrypted := make([]byte, len(input))
	chacha20.XORKeyStream(encrypted, input)
	var dst bytes.Buffer
	dst.Write(IV)
	dst.Write(encrypted)
	return strings.ToUpper(hex.EncodeToString(dst.Bytes()))
}
