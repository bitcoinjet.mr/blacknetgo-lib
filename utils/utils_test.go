package utils

import (
	"testing"
)

func Test_Blacknet_Utils_EncodeVarInt_DecodeVarInt(t *testing.T) {
	l := 1000
	res := EncodeVarInt(l)
	expect := DecodeVarInt(res)
	if expect != l {
		t.Errorf("EncodeVarInt(%v) = %v; expected %v", l, res, expect)
	}
}

func Benchmark_Blacknet_Utils_EncodeVarInt(b *testing.B) {
	l := 1000
	for i := 0; i < b.N; i++ {
		l += i
		EncodeVarInt(l)
	}
}

func Benchmark_Blacknet_Utils_DecodeVarInt(b *testing.B) {
	res := []byte{7, 232}
	for i := 0; i < b.N; i++ {
		DecodeVarInt(res)
	}
}
