package utils

import (
	"bytes"
	"encoding/binary"
	"encoding/hex"
	"strings"

	"gitlab.com/blacknet-ninja/blacknetgo-lib/ed25519"

	"gitlab.com/blacknet-ninja/blacknetgo-lib/bech32"
)

// PublicKey account string to publickkey
func PublicKey(account string) ed25519.PublicKey {
	_, address, _ := bech32.Decode(account)
	pk, _ := bech32.ConvertBits(address, 5, 8, false)
	return pk
}

// PublicKeyToHex account string to publickkey hex
func PublicKeyToHex(arr []byte) string {
	return strings.ToUpper(hex.EncodeToString(arr))
}

// HexToPublicKey account string to publickkey hex
func HexToPublicKey(str string) []byte {
	pk, _ := hex.DecodeString(strings.ToLower(str))
	return pk
}

// ToHex account string to publickkey hex
func ToHex(arr []byte) string {
	return strings.ToUpper(hex.EncodeToString(arr))
}

// ToByte account string to publickkey hex
func ToByte(str string) []byte {
	pk, _ := hex.DecodeString(strings.ToLower(str))
	return pk
}

// NumberOfLeadingZeros .
func NumberOfLeadingZeros(i int) int {
	// HD, Figure 5-6
	if i == 0 {
		return 32
	}
	n := int(1)
	if i>>16 == 0 {
		n += 16
		i <<= 16
	}
	if i>>24 == 0 {
		n += 8
		i <<= 8
	}
	if i>>28 == 0 {
		n += 4
		i <<= 4
	}
	if i>>30 == 0 {
		n += 2
		i <<= 2
	}
	n -= i >> 31
	return n
}

// EncodeVarInt .
func EncodeVarInt(value int) []byte {
	var shift = 31 - NumberOfLeadingZeros(value)
	shift -= shift % 7 // round down to nearest multiple of 7
	buf := &bytes.Buffer{}
	for {
		if shift == 0 {
			break
		}
		binary.Write(buf, binary.BigEndian, uint8(value>>shift&0x7F))
		shift -= 7
	}
	binary.Write(buf, binary.BigEndian, uint8(value&0x7F|0x80))
	return buf.Bytes()
}

// DecodeVarInt .
func DecodeVarInt(arr []byte) int {
	var ret int
	offset := 0
	for {
		buf := bytes.NewReader(arr[offset : offset+1])
		var v uint8
		binary.Read(buf, binary.BigEndian, &v)
		ret = ret<<7 | int((v & 0x7F))
		if v&0x80 != 0 {
			break
		}
		offset++
	}
	return ret
}
