package jsonrpc

import (
	"strconv"
	"strings"

	"gitlab.com/blacknet-ninja/blacknetgo-lib/blacknet/hash"
	"gitlab.com/blacknet-ninja/blacknetgo-lib/blacknet/message"
	"gitlab.com/blacknet-ninja/blacknetgo-lib/blacknet/options"
	"gitlab.com/blacknet-ninja/blacknetgo-lib/blacknet/transaction"
	"gitlab.com/blacknet-ninja/blacknetgo-lib/blacknet/transaction/cancellease"
	"gitlab.com/blacknet-ninja/blacknetgo-lib/blacknet/transaction/lease"
	"gitlab.com/blacknet-ninja/blacknetgo-lib/blacknet/transaction/transfer"
	"gitlab.com/blacknet-ninja/blacknetgo-lib/blacknet/transaction/withdrawfromlease"
	"gitlab.com/blacknet-ninja/blacknetgo-lib/utils"
)

// Serialize .
type Serialize struct {
	Endpoint string
	Request  *Request
}

// NewSerialize new NewSerialize
func NewSerialize(endpoint string) *Serialize {
	return &Serialize{
		Endpoint: endpoint,
		Request:  NewRequest(),
	}
}

// Transfer . url.Values{"username": {"auto"}, "password": {"auto123123"}}
func (s *Serialize) Transfer(data options.Transfer) *Body {
	body := s.GetSeq(data.From)
	if body.Code != 200 {
		return body
	}
	m := message.New(message.PLAIN, data.Message)

	d := transfer.New(data.Amount, data.To, m)

	seq, _ := strconv.ParseUint(body.Body, 10, 64)

	t := transaction.New(data.From, uint32(seq), hash.Empty(), data.Fee, 0, d.Serialize())

	return &Body{Code: 200, Body: utils.ToHex(t.Serialize())}
}

// Lease . url.Values{"username": {"auto"}, "password": {"auto123123"}}
func (s *Serialize) Lease(data options.Lease) *Body {
	body := s.GetSeq(data.From)
	if body.Code != 200 {
		return body
	}
	d := lease.New(data.Amount, data.To)

	seq, _ := strconv.ParseUint(body.Body, 10, 64)

	t := transaction.New(data.From, uint32(seq), hash.Empty(), data.Fee, 2, d.Serialize())

	return &Body{Code: 200, Body: utils.ToHex(t.Serialize())}
}

// CancelLease . url.Values{"username": {"auto"}, "password": {"auto123123"}}
func (s *Serialize) CancelLease(data options.CancelLease) *Body {
	body := s.GetSeq(data.From)
	if body.Code != 200 {
		return body
	}
	d := cancellease.New(data.Amount, data.To, data.Height)

	seq, _ := strconv.ParseUint(body.Body, 10, 64)

	t := transaction.New(data.From, uint32(seq), hash.Empty(), data.Fee, 3, d.Serialize())

	return &Body{Code: 200, Body: utils.ToHex(t.Serialize())}
}

// WithdrawFromLease .
func (s *Serialize) WithdrawFromLease(data options.WithdrawFromLease) *Body {
	body := s.GetSeq(data.From)
	if body.Code != 200 {
		return body
	}
	d := withdrawfromlease.New(data.Withdraw, data.Amount, data.To, data.Height)

	seq, _ := strconv.ParseUint(body.Body, 10, 64)

	t := transaction.New(data.From, uint32(seq), hash.Empty(), data.Fee, 11, d.Serialize())

	return &Body{Code: 200, Body: utils.ToHex(t.Serialize())}
}

// GetSeq . url.Values{"username": {"auto"}, "password": {"auto123123"}}
func (s *Serialize) GetSeq(account string) *Body {
	return s.Request.Get(strings.Join([]string{
		s.Endpoint + "/api/v1/walletdb/getsequence",
		account,
	}, "/"))
}
