package jsonrpc

import (
	"testing"

	"gitlab.com/blacknet-ninja/blacknetgo-lib/blacknet/options"
)

var serialize = NewSerialize("https://blnmobiledaemon.blnscan.io")

func Test_Blacknet_Serialize_Transfer(t *testing.T) {
	amount := uint64(1e8)
	fee := uint64(0.001 * 1e8)
	// data := url.Values{"from": {testAccount2}, "to": {testAccount2}, "amount": {strconv.FormatInt(amount, 10)}, "fee": {strconv.FormatInt(fee, 10)}}
	data := options.Transfer{
		From:      testAccount2,
		To:        testAccount2,
		Amount:    amount,
		Fee:       fee,
		Message:   "xxxx",
		Encrypted: 1,
	}
	res := serialize.Transfer(data)
	if res.Code != 200 {
		t.Errorf("Serialize.Transfer(%v) = %v; expected %v", data, res.Body, res.Code)
	}
}

func Test_Blacknet_Serialize_Lease(t *testing.T) {
	amount := uint64(1e8)
	fee := uint64(0.001 * 1e8)
	data := options.Lease{
		From:   testAccount2,
		To:     testAccount2,
		Amount: amount,
		Fee:    fee,
	}
	res := serialize.Lease(data)
	if res.Code != 200 {
		t.Errorf("Serialize.Lease(%v) = %v; expected %v", data, res.Body, res.Code)
	}
}

func Test_Blacknet_Serialize_CancelLease(t *testing.T) {
	amount := uint64(1e8)
	fee := uint64(0.001 * 1e8)
	data := options.CancelLease{
		From:   testAccount2,
		To:     testAccount2,
		Amount: amount,
		Fee:    fee,
		Height: 1111111,
	}
	res := serialize.CancelLease(data)
	if res.Code != 200 {
		t.Errorf("Serialize.CancelLease(%v) = %v; expected %v", data, res.Body, res.Code)
	}
}

func Test_Blacknet_Serialize_WithdrawFromLease(t *testing.T) {
	amount := uint64(1e8)
	withdraw := uint64(1e8)
	fee := uint64(0.001 * 1e8)
	data := options.WithdrawFromLease{
		Withdraw: withdraw,
		From:     testAccount2,
		To:       testAccount2,
		Amount:   amount,
		Fee:      fee,
		Height:   1111111,
	}
	res := serialize.WithdrawFromLease(data)
	if res.Code != 200 {
		t.Errorf("Serialize.WithdrawFromLease(%v) = %v; expected %v", data, res.Body, res.Code)
	}
}
