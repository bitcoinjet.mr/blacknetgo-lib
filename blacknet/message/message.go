package message

import (
	"bytes"
	"encoding/binary"

	"gitlab.com/blacknet-ninja/blacknetgo-lib/utils"
)

const (
	// PLAIN .
	PLAIN = uint8(0)
	// ENCRYPTED .
	ENCRYPTED = uint8(1)
)

// Message .
type Message struct {
	Type    uint8
	Message string
}

// New new a transfer
func New(t uint8, message string) Message {
	return Message{
		Type:    t,
		Message: message,
	}
}

// Empty .
func Empty() *Message {
	return &Message{
		Type: 0,
	}
}

// Serialize .
func (s Message) Serialize() []byte {
	buf := &bytes.Buffer{}
	// type
	binary.Write(buf, binary.BigEndian, s.Type)
	// message
	binary.Write(buf, binary.BigEndian, utils.EncodeVarInt(len([]byte(s.Message))))
	binary.Write(buf, binary.BigEndian, []byte(s.Message))
	return buf.Bytes()
}

// Deserialize .
func Deserialize(arr []byte) Message {
	var t uint8
	buf := bytes.NewReader(arr[0:1])
	binary.Read(buf, binary.BigEndian, &t)
	return Message{
		Type:    t,
		Message: string(arr[len(arr)-utils.DecodeVarInt(arr[1:]):]),
	}
}
