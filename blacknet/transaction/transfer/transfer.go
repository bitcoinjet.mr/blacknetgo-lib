package transfer

import (
	"bytes"
	"encoding/binary"

	"gitlab.com/blacknet-ninja/blacknetgo-lib/blacknet/message"
	"gitlab.com/blacknet-ninja/blacknetgo-lib/utils"
)

// Transfer .
type Transfer struct {
	Amount    uint64
	PublicKey string
	Message   message.Message
}

// New new a transfer
func New(amount uint64, to string, message message.Message) *Transfer {
	return &Transfer{
		Amount:    amount,
		PublicKey: utils.PublicKeyToHex(utils.PublicKey(to)),
		Message:   message,
	}
}

// Serialize .
func (s *Transfer) Serialize() []byte {
	buf := &bytes.Buffer{}
	// amount
	binary.Write(buf, binary.BigEndian, s.Amount)
	// publickey
	binary.Write(buf, binary.BigEndian, utils.HexToPublicKey(s.PublicKey))
	// message
	binary.Write(buf, binary.BigEndian, s.Message.Serialize())
	return buf.Bytes()
}

// Deserialize .
func Deserialize(arr []byte) *Transfer {
	return &Transfer{
		Amount:    binary.BigEndian.Uint64(arr[0:8]),
		PublicKey: utils.PublicKeyToHex(arr[8:40]),
		Message:   message.Deserialize(arr[40:]),
	}
}
