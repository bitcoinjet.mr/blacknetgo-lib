package lease

import (
	"bytes"
	"encoding/binary"

	"gitlab.com/blacknet-ninja/blacknetgo-lib/utils"
)

// Lease .
type Lease struct {
	Amount    uint64
	PublicKey string
}

// New new a lease
func New(amount uint64, to string) *Lease {
	return &Lease{
		Amount:    amount,
		PublicKey: utils.PublicKeyToHex(utils.PublicKey(to)),
	}
}

// Serialize .
func (s *Lease) Serialize() []byte {
	buf := &bytes.Buffer{}
	// amount
	binary.Write(buf, binary.BigEndian, s.Amount)
	// publickey
	binary.Write(buf, binary.BigEndian, utils.HexToPublicKey(s.PublicKey))
	return buf.Bytes()
}

// Deserialize .
func Deserialize(arr []byte) *Lease {
	return &Lease{
		Amount:    binary.BigEndian.Uint64(arr[0:8]),
		PublicKey: utils.PublicKeyToHex(arr[8:40]),
	}
}
