package cancellease

import (
	"bytes"
	"encoding/binary"

	"gitlab.com/blacknet-ninja/blacknetgo-lib/utils"
)

// CancelLease .
type CancelLease struct {
	Amount uint64
	To     string
	Height uint32
}

// New new a cancellease
func New(amount uint64, to string, height uint32) *CancelLease {
	return &CancelLease{
		Amount: amount,
		To:     utils.PublicKeyToHex(utils.PublicKey(to)),
		Height: height,
	}
}

// Serialize .
func (s *CancelLease) Serialize() []byte {
	buf := &bytes.Buffer{}
	// amount
	binary.Write(buf, binary.BigEndian, s.Amount)
	// to
	binary.Write(buf, binary.BigEndian, utils.HexToPublicKey(s.To))
	// height
	binary.Write(buf, binary.BigEndian, s.Height)
	return buf.Bytes()
}

// Deserialize .
func Deserialize(arr []byte) *CancelLease {
	return &CancelLease{
		Amount: binary.BigEndian.Uint64(arr[0:8]),
		To:     utils.PublicKeyToHex(arr[8:40]),
		Height: binary.BigEndian.Uint32(arr[40:]),
	}
}
