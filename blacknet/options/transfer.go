package options

// Transfer .
type Transfer struct {
	Amount    uint64
	From      string
	To        string
	Fee       uint64
	Message   string
	Encrypted int
}

// Lease .
type Lease struct {
	Amount uint64
	From   string
	To     string
	Fee    uint64
}

// CancelLease .
type CancelLease struct {
	Amount uint64
	From   string
	To     string
	Fee    uint64
	Height uint32
}

// WithdrawFromLease .
type WithdrawFromLease struct {
	Withdraw uint64
	Amount   uint64
	From     string
	To       string
	Fee      uint64
	Height   uint32
}
